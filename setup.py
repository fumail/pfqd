# -*- coding: UTF-8 -*-
import sys
if sys.version_info < (3,6):
    print('Python >= 3.6 required. Python %s.%s found' % (sys.version_info[0], sys.version_info[1]))
    sys.exit(1)
    

sys.path.insert(0,'src')
from pfqd import __version__
import os
import glob
from setuptools import find_packages, setup

data_files = [
    ('/etc/pfqd',glob.glob('conf/*.dist')),
    ('/usr/lib/systemd/system/', ['systemd/pfqd.service']),
]

setup(
    name='pfqd',
    version = __version__,
    description = 'Distributed Postfix Queue Daemon',
    author = 'Fumail Project',
    url='http://www.fuglu.org',
    author_email = 'foss@neonknight.ch',
    package_dir={'':'src'},
    packages = find_packages('src'),
    scripts=[],
    long_description = 'Distributed Postfix Queue Daemon',
    requires=[
        'redis',
    ],
    data_files=data_files,
    entry_points={
          'console_scripts': [
              'pfqd = pfqd.__main__:main',
          ]
      },
    options = {'bdist_rpm':{'post_install' : 'systemd/rpmpost.sh'}},
)
