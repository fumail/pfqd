# -*- coding: utf-8 -*-
#   Copyright 2020-2022 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import logging
import time
import base64
import pickle
import redis
import typing as tp
import argparse
import re


class QStore(object):
    QUEUE_ACTIVE = 'active'
    QUEUE_DEFERRED = 'deferred'
    QUEUE_HOLD = 'hold'
    QUEUE_INCOMING = 'incoming'
    
    _queues = [QUEUE_ACTIVE, QUEUE_DEFERRED, QUEUE_HOLD, QUEUE_INCOMING]
    
    
    
    def __init__(self, args:argparse.Namespace):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.ttl = args.ttl
        self.hostname = args.hostname
        self.redisconn = args.redisconn
        self.redis_pool = None
        
        
        
    def _get_redis(self) -> redis.StrictRedis:
        if self.redis_pool is None:
            self.redis_pool = redis.ConnectionPool(socket_keepalive=True, socket_timeout=5, socket_connect_timeout=5, health_check_interval=10, client_name='pfqd')
            self.redis_pool = self.redis_pool.from_url(self.redisconn, retry_on_timeout=True)
        redisconn = redis.StrictRedis(connection_pool=self.redis_pool)
        return redisconn
    
    
    
    @staticmethod
    def _b64pickle(value:tp.Any) -> bytes:
        return base64.b64encode(pickle.dumps(value, pickle.DEFAULT_PROTOCOL))
    
    
    @staticmethod
    def _b64unpickle(value:bytes) -> tp.Any:
        return pickle.loads(base64.b64decode(value))
    
    
    
    def _check_queue_name(self, queue_name:str) -> bool:
        ok = True
        if queue_name not in self._queues:
            self.logger.info(f'queue_name {queue_name} not one of {",".join(self._queues)}')
            ok = False
        return ok
    
    
    @staticmethod
    def _check_hostrgx(hostrgx:re.Pattern) -> None:
        if hostrgx is not None:
            if hostrgx.__class__.__name__ not in ['Pattern', 'SRE_Pattern']:
                # not the pythonic way, but we avoid import of _sre
                # python 3.6: _sre.SRE_Pattern
                # python >= 3.7: re.Pattern
                raise TypeError(f'hostrx is not of type re.Pattern type is {hostrgx.__class__.__name__}')
            if not isinstance(hostrgx.pattern, bytes):
                raise TypeError('hostrgx pattern must be of type bytes')
    
    
    
    def set_summary(self, queue_name:str, summary_table:tp.List[tp.Tuple[str,str,str,str,str]], retry:int=3) -> None:
        if not self._check_queue_name(queue_name):
            return
        if summary_table:
            try:
                redisconn = self._get_redis()
                ts = time.time()
                redisconn.hset(f'pfqdsum_{queue_name}', self.hostname, self._b64pickle((ts, summary_table)))
                self.logger.debug(f'summary: stored {len(summary_table)} entries for queue {queue_name}')
            except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError, redis.exceptions.ResponseError) as e:
                if retry>0:
                    time.sleep(0.1*(4-retry))
                    self.set_summary(queue_name, summary_table, retry=retry-1)
                else:
                    self.logger.error(f'summary: error storing queue{queue_name}: {str(e)}')
        else:
            self.logger.debug(f'summary: nothing to store for queue {queue_name}')
    
    
    
    def get_summary(self, queue_name:str, hostrgx:re.Pattern=None) -> tp.List[tp.Tuple[str,str,str,str,str]]:
        """
        get queue summary for queue queue_name and optionally hosts defined by hostrgx
        :param queue_name: name of queue (active, deferred, hold)
        :param hostrgx: regular expression defining hosts for which to get queue
        :return: list of host queue values
        """
        summary_table = []
        if not self._check_queue_name(queue_name):
            return summary_table
        self._check_hostrgx(hostrgx)
        
        redisconn = self._get_redis()
        try:
            data = redisconn.hgetall(f'pfqdsum_{queue_name}')
        except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError, redis.exceptions.ResponseError) as e:
            self.logger.error(f'failed to get summary due to {str(e)}')
            data = None
        if data is None:
            self.logger.error(f'summary: no data retrieved for queue {queue_name}')
            return summary_table
        exp = time.time()-self.ttl
        for hostname in data:
            if hostrgx is None or hostrgx.match(hostname):
                ts, host_table = self._b64unpickle(data[hostname])
                if ts > exp:
                    summary_table.extend(host_table)
                else:
                    self.logger.info(f'summary: ignoring stale data from {hostname.decode()} for queue {queue_name}')
        return summary_table
    
    
    
    def set_full(self, queue_name:str, queue_data:tp.List[tp.Dict[str,tp.Any]], retry:int=3) -> None:
        if not self._check_queue_name(queue_name):
            return
        if queue_data:
            try:
                redisconn = self._get_redis()
                ts = time.time()
                redisconn.hset(f'pfqd_{queue_name}', self.hostname, self._b64pickle((ts, queue_data)))
                self.logger.debug(f'full: stored {len(queue_data)} entries for queue {queue_name}')
            except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError, redis.exceptions.ResponseError) as e:
                if retry>0:
                    time.sleep(0.1*(4-retry))
                    self.set_full(queue_name, queue_data, retry=retry-1)
                else:
                    self.logger.error(f'full: error storing queue{queue_name}: {str(e)}')
        else:
            self.logger.debug(f'full: nothing to store for queue {queue_name}')
    
    
    
    def get_full(self, queue_name:str, hostrgx:re.Pattern=None) -> tp.List[tp.Dict[str,tp.Any]]:
        """
        get full queue for queue queue_name and optionally hosts defined by hostrgx
        :param queue_name: name of queue (active, deferred, hold)
        :param hostrgx: regular expression defining hosts for which to get queue
        :return: list of host queue values
        """
        
        queue_data = []
        if not self._check_queue_name(queue_name):
            return queue_data
        self._check_hostrgx(hostrgx)
        
        redisconn = self._get_redis()
        try:
            data = redisconn.hgetall(f'pfqd_{queue_name}')
        except (redis.exceptions.ConnectionError, redis.exceptions.TimeoutError, redis.exceptions.ResponseError) as e:
            self.logger.error(f'failed to get data due to {str(e)}')
            data = None
        if data is None:
            self.logger.error(f'full: no data retrieved for queue {queue_name}')
            return queue_data
        exp = time.time()-self.ttl
        for hostname in data:
            if hostrgx is None or hostrgx.match(hostname):
                ts, host_table = self._b64unpickle(data[hostname])
                if ts > exp:
                    for item in host_table:
                        item['hostname'] = hostname.decode()
                    queue_data.extend(host_table)
                else:
                    self.logger.info(f'full: ignoring stale data from {hostname.decode()} for queue {queue_name}')
        return queue_data
    
    
    