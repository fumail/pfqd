# -*- coding: UTF-8 -*-
#   Copyright 2020-2022 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import time
import logging
from .qreader import QReader
from .qstore import QStore
from .qtools import summary_table



DEFAULT_TTL=60



class PFQD(object):
    def __init__(self, args):
        self.args = args
        self.logger = logging.getLogger(self.__class__.__name__)
        
    
    
    def start(self):
        try:
            qreader = QReader()
            qstore = QStore(self.args)
            while True:
                self.logger.debug('processing')
                queues = qreader.read()
                for queue_name in queues:
                    queue = queues[queue_name]
                    summary = summary_table(queue)
                    self.logger.debug(f'{queue_name} summary size {len(summary)}')
                    qstore.set_summary(queue_name, summary)
                    qstore.set_full(queue_name, queue)
                time.sleep(self.args.refresh)
        except KeyboardInterrupt:
            self.logger.info('shutting down')