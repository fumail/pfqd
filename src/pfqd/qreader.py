# -*- coding: utf-8 -*-
#   Copyright 2020-2022 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
import logging
import subprocess
import json
import typing as tp


IGNORELINES = {
    b'postqueue: fatal: malformed showq server response',
    b'postqueue: warning: Mail system is down -- accessing queue directly',
}
"postqueue: warning: Mail system is down -- accessing queue directly (Connect to the Postfix showq service: Connection refused)"


class QReader(object):
    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
    

    def _read_queue(self) -> tp.List[bytes]:
        try:
            with subprocess.Popen(['/usr/sbin/postqueue', '-j'],stdout=subprocess.PIPE) as proc:
                stdout, _ = proc.communicate()
                lines = stdout.splitlines()
                self.logger.debug(f'read {len(lines)} lines')
        except Exception as e:
            self.logger.error(f'failed to call postqueue due to {e.__class__.__name__}: {str(e)}')
            lines = []
        return lines
    
    
    def _parse_json(self, lines:tp.List[bytes]) -> tp.Dict[str, tp.List[tp.Dict[str,tp.Any]]]:
        queues = {}
        for line in lines:
            # ignore lines - partial match is enough
            if any(iline in line for iline in IGNORELINES):
                self.logger.debug(f'ignoring queue line: "{line}"')
                continue
            try:
                m = json.loads(line)
                try:
                    queues[m['queue_name']].append(m)
                except KeyError:
                    queues[m['queue_name']] = [m]
            except Exception as e:
                self.logger.error(f'Failed to parse queue line "{line.decode()}": {e.__class__.__name__}: {str(e)}')
        return queues
    
    
    
    def read(self) -> tp.Dict[str, tp.List[tp.Dict[str,tp.Any]]]:
        lines = self._read_queue()
        queues = self._parse_json(lines)
        return queues
    

