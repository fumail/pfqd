pfqd is a simple postfix queue reader daemon. 

It regularly reads postfix queue listings and writes queue listing data in full 
and summarized form to a redis database. This data can then be read and aggregated,
thus providing queue information for several servers in one central location.

By default it starts the daemon which needs at least needs the redis connection information:

`python3 -m pfqd --redisconn redis://server:port/dbid`

alternatively the redis connection can be defined in /etc/pfqd/pfqd.ini or via 
environment variable REDIS_CONN

when running in docker you may want to specify a host name, either via command 
line parameter --hostname, config file or environment variable SWARM_NODE
